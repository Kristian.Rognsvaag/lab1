package rockPaperScissors;

import java.util.Arrays;
import java.util.List;
import java.util.Scanner;
import java.util.Random;

public class RockPaperScissors {
	
	public static void main(String[] args) {
    	/* 	
    	 * The code here does two things:
    	 * It first creates a new RockPaperScissors -object with the
    	 * code `new RockPaperScissors()`. Then it calls the `run()`
    	 * method on the newly created object.
         */
        new RockPaperScissors().run();
    }
    
    
    Scanner sc = new Scanner(System.in);
    int roundCounter = 1;
    int humanScore = 0;
    int computerScore = 0;
    List<String> rpsChoices = Arrays.asList("rock", "paper", "scissors");
    
    public void run() {
        boolean running = true;
        boolean sanity = false;

        while (running) {
            // Out
            if (!sanity) {
                System.out.println("Let's play round " + roundCounter);
            } else {
                sanity = false;
            }

            // In
            String hi = readInput("Your choice (Rock/Paper/Scissors)?");

            // Sanity check
            if (!rpsChoices.contains(hi)) {
                System.out.println("I do not understand "+ hi + ". Could you try again? ");
                sanity = true;
            } else {
                // Computer's choice
                String mi = rpsChoices.get(new Random().nextInt(rpsChoices.size()));

                // Tie check
                if (hi.equals(mi)) {
                    System.out.println("Human chose " + hi + ", computer chose " + mi + ". It's a tie!");
                } else {
                    String winner = rpcWinner(hi, mi) ? "Human" : "Computer";

                    // Score
                    if (winner == "Human") {
                        humanScore++;
                    } else {
                        computerScore++;
                    }

                    System.out.println("Human chose " + hi + ", computer chose " + mi + ". " + winner + " wins!");
                }

                // Print score
                System.out.println("Score: human " + humanScore + ", computer " + computerScore);

                // Game Loop
                String exitQuery = readInput("Do you wish to continue playing? (y/n)?");
                if (exitQuery.equals(new String("n"))) {
                    System.out.println("Bye bye :)");
                    running = false;
                } else {
                    roundCounter++;
                }
            }
        }
    }

    /**
     * Reads input from console with given prompt
     * @param prompt
     * @return string input answer from user
     */
    public String readInput(String prompt) {
        System.out.println(prompt);
        String userInput = sc.next();
        return userInput;
    }

    /**
     * Gets two rock, paper, scissors inputs 
     * and determines who won.
     * @param choice1
     * @param choice2
     * @return true if the first choice wins
     */
    public boolean rpcWinner(String choice1, String choice2) {
        if (choice1.equals(new String("paper"))) {
            return choice2.equals(new String("rock"));
        } else if (choice1.equals(new String("scissors"))) {
            return choice2.equals(new String("paper"));
        } else {
            return choice2.equals(new String("scissors"));
        }
    }

}
